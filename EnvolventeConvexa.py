# Envolvente convexa

import random as rand
import numpy as np
import matplotlib.pyplot as plt

# FUNCIONES

def giro_horario():
    array = [coord_puntos[0], coord_puntos[1]]
    for i in range(2, len(coord_puntos)):
        array.append(coord_puntos[i])
        while len(array) > 2 and np.linalg.det([array[-3], array[-2], array[-1]]) > 0:
            array.pop(-2)
    return array

def convex_hull():
    coord_puntos.sort()
    l_superior = giro_horario()
    coord_puntos.reverse()
    l_inferior = giro_horario()
    l = l_superior + l_inferior
    return l

def grafica(pol_convexo, coord_puntos):
    # Acomodando listas adecuadas para graficar en matplot
    puntos_x = [i[0] for i in coord_puntos]
    puntos_y = [i[1] for i in coord_puntos]
    poligonos_x = [i[0] for i in pol_convexo]
    poligonos_y = [i[1] for i in pol_convexo]

    # Definiendo límites extremos de la gráfica
    x_lim_der = max(puntos_x) + 5
    y_lim_sup = max(puntos_y) + 5
    x_lim_izq = min(puntos_x) - 5
    y_lim_inf = min(puntos_y) - 5

    # Asignación de los límites extremos
    plt.xlim(x_lim_izq, x_lim_der)
    plt.ylim(y_lim_inf, y_lim_sup)

    # Graficación
    plt.title('Envolvente Convexa')
    plt.plot(puntos_x, puntos_y, 'ko')
    plt.plot(poligonos_x, poligonos_y, 'r-', linewidth = 2.0)
    plt.show()


# Generación de coordenadas de manera aleatoria
num_puntos = 10
coord_puntos = []
for i in range(num_puntos):
    coord_puntos.append([rand.uniform(0, 50), rand.uniform(0, 100), 1.0])
    
# Creación y graficación del polígono convexo
pol_convexo = convex_hull()
grafica(pol_convexo, coord_puntos)
